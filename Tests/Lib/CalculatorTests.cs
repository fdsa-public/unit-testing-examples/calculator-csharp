using System;
using Lib;
using Xunit;

namespace Tests.Lib
{
    public class CalculatorTests
    {
        [Fact]
        public void Given_two_numbers_When_add_Then_it_should_calculate_the_sum_of_them()
        {
            var calculator = new Calculator();

            var result = calculator.Add(1, 2);

            Assert.Equal(3, result);
        }
        
        [Fact]
        public void Given_two_numbers_When_substract_Then_it_should_calculate_the_difference_between_them()
        {
            var calculator = new Calculator();

            var result = calculator.Substract(3, 7);

            Assert.Equal(-4, result);
        }
    }
}
